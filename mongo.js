// What is MongoDB Atlas?
	// MongoDB Atlas is a MongoDB database in the cloud. It is the cloud service of MongoDB, the leading noSQL database.

// What is Robo3T?
	// Robo3T is an application which allows the use of GUI to manipulate MongoDB databases. The advantage of Robo3T in just using Atlas is that Robo3T is a local application

// What is Shell?
	// Shell is an interface in MongoDB that allows us to input commands and perform CRUD operations in our database



//CRUD Operations
	//Create - inserting documents
	/*
		db.colection.insertOne({
			"fieldA" : "value A",
			"fieldB" : "value B"
		}) - allows us to insert a document into a collection
	*/

		//insertOne
		db.users.insertOne({

			"firstName" : "Jean",
			"lastName" : "Smith",
			"age" : 24,
			"contact" : {
				"phone" : "091234567789",
				"email" : "js@mail.com"
			},
			"courses" : ["CSS", "Javascript", "ReactJS"],
			"department" : "none"
		})

		//insertMany({})
			/*
				syntax:
				db.collections.insertMany([
					{objectA}, {objectB}
				])
			*/
		db.users.insertMany([
				{
					"firstName" : "Stephen",
					"lastName" : "Hawking",
					"age" : 76,
					"contact" : {
						"phone" : "09153456789",
						"email" : "sh@mail.com"
					},
					"courses" : ["Phyton", "PHP", "React"],
					"department" : "none"
				},

				{
					"firstName" : "Neil",
					"lastName" : "Armstrong",
					"age" : 82,
					"contact" : {
						"phone" : "09157654321",
						"email" : "na@mail.com"
					},
					"courses" : ["Laravel", "Sass", "Springboot"],
					"department" : "none"
				}
			])

	//read - find documents
		/*
			syntax:
			db.collection.find()
				>>will return ALL documents
			db.collection.findOne({})
				>>will return the first document
			db.collection.find({"criteria" : "value"})
				>>will return ALL documents that matches the criteria in the collection
			db.collection.findOne({"criteria" : "value"})
				>>will return the first document that matches the criteria
			db.collection.find({"fieldA" : "valueA", "fieldB" : "valueB"})
		*/

		db.users.find()

		db.users.findOne({})

		db.users.find({"firstName" : "Jean"})

		db.users.findOne({"firstName" : "Kate"})

		db.users.find({"lastName" : "Armstrong", "age" : 82})

	//update documents
		/*
			syntax:
			db.collection.updateOne({"citeria" : "value"}, {$set:{"fieldToBeUpdated" : "updatedValue"}})
				>>to update the first item that matches the criteria

			db.collection.updateOne({}, {$set:{"fieldToBeUpdated" : "updatedValue"}})

			db.collection.updateMany({"citeria" : "value"}, {$set:{"fieldToBeUpdated" : "updatedValue"}})

			db.collection.updateMany({}, {$set:{"fieldToBeUpdated" : "updatedValue"}})
		*/
	db.users.updateOne({"firstName" : "Jean"}, {$set : {"lastName" : "Gates"}})

	db.users.updateOne({}, {$set:{"emergencyContact" : "father"}})

	db.users.updateOne({"department" : "none"}, {$set:{"department" : "tech"}})

	db.users.updateMany({}, {$set:{"emergencyContact" : "mother"}})

	// Delete- to delete documents
	/*
		syntax:

			db.collection.deleteOne({"criteria" : "value"})
				>>delete the first item that matches the criteria

			db.collection.deleteMany({"criteria" : "value"})
				>>delete ALL item that matches the criteria

			db.collection.deleteOne({})
				>>delete the first item in the collection

			db.collection.deleteMany({})
				>>delete ALL items in the collection
	*/

	db.users.insertOne({
		"name" : "Kim Jin",
		"birthday" : "12/30/1992",
		"age" : 29
	})