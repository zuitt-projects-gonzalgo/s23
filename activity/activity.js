//Add Collections
db.users.insertMany([
	{
		"firstName" : "Luffy",
		"lastName" : "Monkey",
		"email" : "strahat@mail.com",
		"password" : "meatmeat",
		"isAdmin" : false,
	},

	{
		"firstName" : "Zoro",
		"lastName" : "Roronoa",
		"email" : "santoryu@mail.com",
		"password" : "sakesake",
		"isAdmin" : false,
	},

	{
		"firstName" : "Nami",
		"lastName" : "",
		"email" : "nami@mail.com",
		"password" : "beriberi",
		"isAdmin" : false,
	},

	{
		"firstName" : "Ussop",
		"lastName" : "God",
		"email" : "ussop@mail.com",
		"password" : "goingmerry",
		"isAdmin" : false,
	},

	{
		"firstName" : "Sanji",
		"lastName" : "Vinsmoke",
		"email" : "sanji@gmail.com",
		"password" : "namichaaan",
		"isAdmin" : false,
	},

	{
		"firstName" : "Chopper",
		"lastName" : "Tonytony",
		"email" : "chopper@gmail.com",
		"password" : "cottoncandy",
		"isAdmin" : false,
	},

	{
		"firstName" : "Robin",
		"lastName" : "Nico",
		"email" : "robin@gmail.com",
		"password" : "voidcentruy",
		"isAdmin" : false,
	},

	{
		"firstName" : "Flam",
		"lastName" : "Cutty",
		"email" : "franky@gmail.com",
		"password" : "colacola",
		"isAdmin" : false,
	},

	{
		"firstName" : "Brook",
		"lastName" : "Soul King",
		"email" : "bones@gmail.com",
		"password" : "caniseeyourpanty",
		"isAdmin" : false,
	},

	{
		"firstName" : "Jinbe",
		"lastName" : "",
		"email" : "jinbe@gmail.com",
		"password" : "fishmanisland",
		"isAdmin" : false,
	}
])

db.courses.insertMany([
		{
			"name" : "Pirate King",
			"price" : 10000000000000,
			"isActive" : false
		},

		{
			"name" : "Strongest Swordsman in the World",
			"price" : 8000000000000,
			"isActive" : false
		},

		{
			"name" : "Find All Blue",
			"price" : 5000000000000,
			"isActive" : false
		},
	])

//Read
db.users.find({"isAdmin" : false})

//Update
db.users.updateOne({}, {$set : {"isAdmin" : true}})

db.courses.updateOne({}, {$set : {"isActive" : true}})

//Delete
db.courses.deleteMany({"isActive" : false})